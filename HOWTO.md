# O que foi realizado

As entidades funcionário e setor já existiam, então foi implementado as funcionalidades de inserir, editar e deletar.
Para funcionário, o seu service foi completado com as demais rotas CRUD além de Read já presente. Para o setor, foi necessário criar totalidade a sua DAO e seu Service CRUD.
Testes de integração com JUnit e JerseyTest para funcionário e setores as rotas CRUD e sua interação com o banco de dados simulando o funcionamento do projeto.

Para as interfaces Vue.js, na tela principal foi adicionado butões de navegação para as telas de cadastro implementado com  arquivos html dentro da pasta "pages" e arquivos de script js na pasta "resources/js" com as funcões e formulários para realizar tais funções.


# Utilizando o projeto

Após verificados os requisitos base Tomcat 9 e Jdk 8, o arquivo HibernateUtil.java organiza as tabelas necessárias no banco de dados, assim o projeto pode ser usado com "Run as > Run on Server" no Eclipse, por exemplo, e os testes JUnit com "Run as > JUnit Test".
