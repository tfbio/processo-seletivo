package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.FuncionarioDAO;
import com.hepta.funcionarios.persistence.SetorDAO;


class FuncionarioServiceTest extends JerseyTest {
	static FuncionarioDAO funcDao;
	static SetorDAO setorDao;
	
	@BeforeAll
	static public void setUpBeforeALL(){
		funcDao = new FuncionarioDAO();
		setorDao = new SetorDAO();
		
		try{
			funcDao.flush();
			setorDao.flush();			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	@BeforeEach
	@Override
	public void setUp() throws Exception {
		super.setUp();
	}    

	@AfterEach
	@Override
	public void tearDown() throws Exception {
		super.tearDown();
		try{
			funcDao.flush();
			setorDao.flush();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	 
	@Override
	public Application configure() {
		return new ResourceConfig(FuncionarioService.class);
	 }

	
	@Test
	void testFuncionarioCreate() {
		Setor setorInit = new Setor("Setor Teste");
		
		try {
			setorDao.save(setorInit);
			Setor setorParam = new Setor(setorInit.getId(), setorInit.getNome());

			Funcionario funcTeste = new Funcionario("Teste", setorParam, 3500.00, "Email Teste", 40);
			Response response = target("/funcionarios")
					.request()
					.post(Entity.entity(funcTeste, MediaType.APPLICATION_JSON));
			assertEquals(200, response.getStatus(), "Should create new Funcionario in database and return status 200");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	void testFuncionarioRead() {
		Response response = target("/funcionarios").request().get();
		assertEquals(200, response.getStatus(), "Should read all Funcionario entries from database and return status 200");
	}

	@Test
	void testFuncionarioUpdate() {
		Setor setorInit = new Setor("Setor Teste");
		
		try {
			setorDao.save(setorInit);
			Setor setorParam = new Setor(setorInit.getId(), setorInit.getNome());
			
			Funcionario funcTeste = new Funcionario("Teste", setorParam, 3500.00, "Email Teste", 40);
			funcDao.save(funcTeste);
			
			
			Funcionario funcTesteAtt = new Funcionario(funcTeste.getId(),"TesteAtt", setorParam, 4000.00, "Email Teste", 41);
			Response response = target("/funcionarios/"+funcTeste.getId())
					.request()
					.put(Entity.entity(funcTesteAtt, MediaType.APPLICATION_JSON));
			assertEquals(200, response.getStatus(), "Should update Funcionario in database and return status 200");
		} catch(Exception e) { System.out.println(e); }
	}

	@Test
	void testFuncionarioDelete() {
		Setor setorInit = new Setor("Setor Teste");
		
		try {
			setorDao.save(setorInit);
			Setor setorParam = new Setor(setorInit.getId(), setorInit.getNome());
			
			Funcionario funcTeste = new Funcionario("Teste", setorParam, 3500.00, "Email Teste", 40);
			funcDao.save(funcTeste);
			Response response = target("/funcionarios/"+funcTeste.getId()).request().delete();
			assertEquals(200, response.getStatus(), "Should delete Funcionario from database and return status 200");
		} catch(Exception e) { System.out.println(e); }
	}
}
