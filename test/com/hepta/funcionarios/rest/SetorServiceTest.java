package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.glassfish.jersey.test.*;
import org.glassfish.jersey.server.ResourceConfig;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.rest.SetorService;
import com.hepta.funcionarios.persistence.FuncionarioDAO;
import com.hepta.funcionarios.persistence.SetorDAO;

class SetorServiceTest extends JerseyTest {
	static FuncionarioDAO funcDao;
	static SetorDAO setorDao;
	
	
	@BeforeAll
	static public void setUpBeforeALL(){
		funcDao = new FuncionarioDAO();
		setorDao = new SetorDAO();
		try{
			funcDao.flush();
			setorDao.flush();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	
	@BeforeEach
	@Override
	public void setUp() throws Exception {
		super.setUp();
	}    

	
	@AfterEach
	@Override
	public void tearDown() throws Exception {
		super.tearDown();
		try{
			funcDao.flush();
			setorDao.flush();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	 
	@Override
	public Application configure() {
		return new ResourceConfig(SetorService.class);
	 }
	

	@Test
	void testSetorCreate() {
		Setor setorTeste = new Setor("setorCreateTest");
		Response response = target("/setores")
				.request()
				.post(Entity.entity(setorTeste, MediaType.APPLICATION_JSON));
		assertEquals(200, response.getStatus(), "Should create new Setor in database and return status 200");
	}
	
	@Test
	void testSetorCreateWithInvalidInput() {
		Setor setorTeste = new Setor("");
		Response response = target("/setores")
				.request()
				.post(Entity.entity(setorTeste, MediaType.APPLICATION_JSON));
		assertEquals(400, response.getStatus(), "Should not allow Setor without name in database and return status 400");	
	}


	@Test
	void testSetorRead() {
		Response response = target("/setores").request().get();
		assertEquals(200, response.getStatus(), "Should read all Setor entries from database and return status 200");
	}

	@Test
	void testSetorUpdate() {
		Setor setorUpdateTest = new Setor("setorUpdateTest");
		try {
			setorDao.save(setorUpdateTest);
		
			Setor setorUpdateTestAtt = new Setor(setorUpdateTest.getId(), "setorUpdateTestAtt");
			Response response = target("/setores/"+setorUpdateTest.getId())
					.request()
					.put(Entity.entity(setorUpdateTestAtt, MediaType.APPLICATION_JSON));
			assertEquals(200, response.getStatus(), "Should update Setor in database and return status 200");
		} catch(Exception e) { System.out.println(e); }
	}

	@Test
	void testSetorDelete() {
		Setor setorDeleteTest = new Setor("setorDeleteTest");
		try {
			setorDao.save(setorDeleteTest);

			Response response = target("/setores/"+setorDeleteTest.getId()).request().delete();
			assertEquals(200, response.getStatus(), "Should delete Setor from database and return status 200");
		} catch(Exception e) { System.out.println(e); }
	}
}
