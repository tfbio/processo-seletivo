package com.hepta.funcionarios.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDAO;

@Path("/setores")
public class SetorService {
		@Context
	    private HttpServletRequest request;

	    @Context
	    private HttpServletResponse response;

	    private SetorDAO dao;

	    public SetorService() {
	        dao = new SetorDAO();
	    }

	    protected void setRequest(HttpServletRequest request) {
	        this.request = request;
	    }
	    
	    
	    /**
	     * Adiciona novo Setor
	     * 
	     * @param Funcionario: Novo Setor
	     * @return response 200 (OK) - Conseguiu adicionar
	     */
	    @Path("/")
	    @Consumes(MediaType.APPLICATION_JSON)
	    @Produces(MediaType.APPLICATION_JSON)
	    @POST
	    public Response SetorCreate(Setor Setor) {
	    	if(Setor==null || Setor.getNome()==null || Setor.getNome()=="") {
				return Response.status(Status.BAD_REQUEST).entity("Faltam campos necessarios para criar setor").build();
			}
	    	
	    	try {
	        	dao.save(Setor);
	    	} catch (Exception e) {
	    		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao criar novo funcionario").build();
	    	}

	        return Response.status(Status.OK).build();
	    }
	    
	    
	    /**
	     * Lista todos os Setores
	     * 
	     * @return response 200 (OK) - Conseguiu listar
	     */
	    @Path("/")
	    @Produces(MediaType.APPLICATION_JSON)
	    @GET
	    public Response SetorRead() {
	        List<Setor> Setor = new ArrayList<>();
	        try {
	        	Setor = dao.getAll();
	        } catch (Exception e) {
	            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Setores").build();
	        }

	        GenericEntity<List<Setor>> entity = new GenericEntity<List<Setor>>(Setor) {
	        };
	        return Response.status(Status.OK).entity(entity).build();
	    }
	    
	    /**
	     * Lista todos os Setores
	     * 
	     * @return response 200 (OK) - Conseguiu listar
	     */
	    @Path("/{nome}")
	    @Produces(MediaType.APPLICATION_JSON)
	    @GET
	    public Response SetorFind(@PathParam("nome") String nome) {
	        List<Setor> Setor = new ArrayList<>();
	        try {
	        	Setor = dao.findByName(nome);
	        	if(Setor.isEmpty()) return Response.status(Status.BAD_REQUEST).entity("Setor nao encontrado").build();
	        } catch (Exception e) {
	            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Setores").build();
	        }


	        Setor setorResponse = Setor.get(0);
	       
	        return Response.status(Status.OK).entity(setorResponse).type(MediaType.APPLICATION_JSON).build();
	    }
	    
	    
	    /**
	     * Atualiza um Setor
	     * 
	     * @param id:          id do Setor
	     * @param Funcionario: Setor atualizado
	     * @return response 200 (OK) - Conseguiu atualizar
	     */
	    @Path("/{id}")
	    @Consumes(MediaType.APPLICATION_JSON)
	    @Produces(MediaType.APPLICATION_JSON)
	    @PUT
	    public Response SetorUpdate(@PathParam("id") Integer id, Setor Setor) { 	
	    	try {
	    		Setor SetorNoBanco = dao.find(id);
	    		if(SetorNoBanco == null) { 
	        		return Response.status(Status.BAD_REQUEST).entity("Setor nao encontrado").build();
	        	}
	        	if(Setor.getId() == null || Setor.getId() != id) {
	        		return Response.status(Status.BAD_REQUEST).entity("ID correto faltando no corpo da requisicao").build();
	        	}
	        	
	    		dao.update(Setor);
	    	} catch (Exception e) {
	    		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao atualizar Setor").build();
	    	}

	        return Response.status(Status.OK).entity("Conseguiu atualizar").build();
	    }
	    
	    
	    /**
	     * Remove um Setor
	     * 
	     * @param id: id do Setor
	     * @return response 200 (OK) - Conseguiu remover
	     */
	    @Path("/{id}")
	    @Produces(MediaType.APPLICATION_JSON)
	    @DELETE
	    public Response SetorDelete(@PathParam("id") Integer id) {
	        try {
	        	Setor SetorNoBanco = dao.find(id);
	        	if(SetorNoBanco == null) { 
	        		return Response.status(Status.BAD_REQUEST).entity("Funcionario nao encontrado").build();
	        	}
	        	
	        	dao.delete(id);
	        } catch (Exception e) {
	    		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar Setor").build();
	    	}
	        
	        return Response.status(Status.OK).entity("Conseguiu deletar").build();
	    }
}
