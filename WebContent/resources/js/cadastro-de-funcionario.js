var app = new Vue({
	el: '#app',
	data: {
		nome: '',
		email: '',
		idade: '',
		salario: '',
		setor: '',
	},
	methods: {
		onSubmit(e){
			e.preventDefault();
			const vm = this;
			const funcionario = {
					nome: e.target.elements.nome.value,
					email: e.target.elements.email.value,
					idade: parseInt(e.target.elements.idade.value),
					salario: parseFloat(e.target.elements.salario.value),
			}
			const setor = e.target.elements.setor.value;

			axios.get(`/funcionarios/rest/setores/${setor}`)
			.then(response => {
				funcionario.setor = response.data;
				axios.post("/funcionarios/rest/funcionarios", funcionario)
				.then(() => {
					alert("Funcionario cadastrado.");
					vm.$refs.funcionarioForm.reset();
				})
			}).catch(function (error) {
				vm.mostraAlertaErro(error, "Erro ao cadastrar funcionario");
			})
		},
		mostraAlertaErro: function(erro, mensagem){
			console.log(erro);
			alert(mensagem);
		},
	}
})