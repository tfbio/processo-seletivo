var app = new Vue({
	el: '#cadastro-setor',
	data: {
		nome: '',
	},
	methods: {
		onSubmit(e){
			e.preventDefault();
			const vm = this;
			const setor = {
					nome: e.target.elements.nome.value,
			}

			axios.post("/funcionarios/rest/setores", setor)
			.then(() => {
				alert("Setor cadastrado.");
				vm.$refs.setorForm.reset();
			})
			.catch(function(error){
				vm.mostraAlertaErro(error, "Erro ao cadastrar setor");
			}) 				
		},
		
		mostraAlertaErro: function(erro, mensagem){
			console.log(erro);
			alert(mensagem);
		},
	}

})
